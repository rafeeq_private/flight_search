const path = require("path");

module.exports = function override(config, env) {
  //do stuff with the webpack config...

  // config webpack alias
  if (config.resolve.alias) {
    config.resolve.alias = {
      ...config.resolve.alias,
      components: path.resolve("src", "./components"),
      screens: path.resolve("src", "./screens"),
      config: path.resolve("src", "./config"),
      core: path.resolve("src", "./redux")
    };
  }

  return config;
};
