import Axios from "axios";
import { API_URL } from "config/config";

/**
 * @returns AxiosInstance
 */
const axios = Axios.create({
    baseURL: API_URL
});

export const get = url => axios.get(url);
export const post = (url, payload) => axios.post(url, payload)
export default axios;