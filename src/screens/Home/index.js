import React from "react";
import FlightSearch from "components/FlightSearch";
import FlightList from "components/FlightList";
import MainContainer from "components/Layouts/MainContainer";
import { Grid, Box } from "@material-ui/core";
import AddFlight from "components/AddFlight";

export default function HomeScreen(props) {
  return (
    <MainContainer>
      <Grid container spacing={2}>
        <Grid
          container
          item
          xs={12}
          direction="row"
          justify="flex-end"
          alignItems="center"
        >
          <Box m={4}>
            <AddFlight />
          </Box>
        </Grid>
        <Grid item xs={12} md={4}>
          <FlightSearch />
        </Grid>
        <Grid item xs={12} md={8}>
          <FlightList />
        </Grid>
      </Grid>
    </MainContainer>
  );
}
