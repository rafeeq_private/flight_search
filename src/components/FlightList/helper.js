import moment from "moment";

export const getVisibleFlights = (flights, filter) => {
  return flights.filter(
    flight =>
      (filter.from ? flight.get("departure") === filter.from : true) &&
      (filter.to ? flight.get("arrival") === filter.to : true) &&
      (filter.departure
        ? moment(flight.get("departureTime") * 1000).isSameOrAfter(
            filter.departure
          )
        : true) &&
      (filter.return
        ? moment(flight.get("departureTime") * 1000).isSameOrBefore(
            filter.return
          )
        : true)
  );
};

export const sortItems = (key, order) => {
  return (first, second) =>
    order === "asc"
      ? first.get(key) - second.get(key)
      : second.get(key) - first.get(key);
};

export const filterFlightType = type => {
  return flight => (type === "all" ? true : flight.get("type") === type);
};

export const getPaginationCount = (list, perPage) => {
  const total = list.size;
  return Math.ceil(total / perPage);
};

export const getFlightList = (list, filter, sortKey, sortOrder, flightType) => {
  return getVisibleFlights(list, filter)
    .sort((firstItem, secondItem) =>
      sortItems(sortKey, sortOrder)(firstItem, secondItem)
    )
    .filter(type => filterFlightType(flightType)(type));
};

export const getFilteredFlights = (
  searchParams,
  flights,
  sortKey,
  sortOrder,
  flightType
) => {
  return getFlightList(flights, searchParams, sortKey, sortOrder, flightType);
};
