import React, { useEffect, useState } from "react";
import {
  Container,
  Paper,
  Box,
  Grid,
  CircularProgress
} from "@material-ui/core";
import { requestFlightList } from "core/actions";
import Pagination from "@material-ui/lab/Pagination";
import FlightListItem from "components/FlightListItem";
import { useSelector, useDispatch } from "react-redux";
import FlightSort from "components/FlightSort";
import FlightTypeFilter from "components/FlightTypeFilter";
import { getFilteredFlights, getPaginationCount } from "./helper";
import _ from "lodash";

const FlightList = props => {
  const dispatch = useDispatch();
  const unOrderdFlightList = useSelector(state => state.flights);
  const form = useSelector(state => state.search);

  const [sortKey, setSortKey] = useState("departureTime"); // default sort
  const [sortOrder, setSortOrder] = useState("asc"); // default sort order
  const [flightType, setFlightType] = useState("all"); // default type
  const [sliceCount, setSliceCount] = useState(0);
  const perPage = 5;
  const flightList = getFilteredFlights(
    form,
    unOrderdFlightList,
    sortKey,
    sortOrder,
    flightType
  );

  useEffect(() => {
    dispatch(requestFlightList());
  }, [dispatch]);

  if (_.isEmpty(form) && unOrderdFlightList.size === 0)
    return (
      <Box
        px={2}
        mt={20}
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <CircularProgress />
      </Box>
    );

  return (
    <Container>
      <Paper>
        <Grid container p={4}>
          <Grid item xs={12}>
            <Box
              px={2}
              mb={2}
              display="flex"
              justifyContent="flex-end"
              alignItems="center"
            >
              <FlightSort
                sortOrder={sortOrder}
                onOrderChange={value => setSortOrder(value)}
                onKeyChange={value => setSortKey(value)}
                sorts={["departureTime", "arrivalTime"]}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
              px={2}
              mb={2}
              display="flex"
              justifyContent="flex-end"
              alignItems="center"
            >
              <FlightTypeFilter
                flightType={flightType}
                onChange={(value) => {
                  setFlightType(value)
                  setSliceCount(0)
                }}
                items={["all", "cheap", "business"]}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            {flightList
              .slice(sliceCount, sliceCount + perPage)
              .map((flightItem, index) => (
                <FlightListItem flight={flightItem} key={index} />
              ))}
          </Grid>
        </Grid>
      </Paper>
      <Box my={4} display="flex" justifyContent="flex-end" alignItems="center">
        <Pagination
          onChange={(_, value) => setSliceCount((value - 1) * perPage)}
          count={getPaginationCount(flightList, perPage)}
          page={Math.ceil(sliceCount/perPage) + 1}
          color="secondary"
          variant="outlined"
        />
      </Box>
    </Container>
  );
};

export default FlightList;
