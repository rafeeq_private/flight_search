import React from "react";
import {
  Card,
  CardContent,
  Box,
  Typography,
  Divider,
  Chip
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";

const useStyles = makeStyles({
  root: {
    margin: "10px 0"
  }
});

const FlightListItem = props => {
  const { flight } = props;
  const classes = useStyles();
  return (
    <Card elevation={0} className={classes.root}>
      <CardContent>
        <Box display="flex" alignItems="center" justifyContent="space-evenly" flexDirection="row">
          <Box display="flex" flexDirection="column">
            <Typography color="textPrimary" variant="h4">
              {moment(flight.get("departureTime")).format("hh:mm a")}
            </Typography>
            <Typography color="textSecondary" variant="h5">
              {flight.get("departure")}
            </Typography>
            <p color="secondary">
              {moment(flight.get("departureTime") * 1000).format(
                "Do MMMM, YYYY"
              )}
            </p>
          </Box>
          <Box display="flex" flexDirection="column" alignItems="center">
            <Chip label={flight.get("type")} color="secondary"></Chip>
          </Box>
          <Box display="flex" flexDirection="column">
            <Typography color="textPrimary" variant="h4">
              {moment(flight.get("arrivalTime")).format("hh:mm a")}
            </Typography>
            <Typography color="textSecondary" variant="h5">
              {flight.get("arrival")}
            </Typography>
            <p color="secondary">
              {moment(flight.get("arrivalTime") * 1000).format("Do MMMM, YYYY")}
            </p>
          </Box>
        </Box>
      </CardContent>
      <Divider />
    </Card>
  );
};

export default FlightListItem;