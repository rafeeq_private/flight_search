import React, {useEffect } from "react";
import { Form, Field, reduxForm } from "redux-form";
import {
  AutoComplete,
  DatePicker,
  RadioGroup,
  Dropdown,
  TextInput,
  Button
} from "components/FormInputs";
import { Container, Grid } from "@material-ui/core";

const formInputMapper = {
  autoComplete: AutoComplete,
  datePicker: DatePicker,
  radioGroup: RadioGroup,
  dropDown: Dropdown,
  text: TextInput,
  button: Button
};

const required = value => (value ? undefined : "This field is required");

const validationMapper = validations => {
  let mapper = {
    required: required
  };

  return validations ? validations.map(item => mapper[item]) : [];
};

const renderFieldset = field => {
  return (
    <Grid key={field.name} item {...field.grid}>
      <Field
        {...field}
        component={formInputMapper[field.type]}
        validate={validationMapper(field.validate)}
      />
    </Grid>
  );
};

function GenericForm(props) {
  const CustomForm = reduxForm({
    form: props.formId,
    // enableReinitialize: true,
    // initialValues: props.initialValues
  })(FormGenerator);

  return <CustomForm {...props} onSubmit={props.onSubmit} />;
}

function FormGenerator(props) {
  const { handleSubmit, fields, initialValues } = props;

  function setInitialValues(){
    props.initialize(initialValues);
  };

  useEffect(() => {
    setInitialValues();
  }, [initialValues]);

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Grid container spacing={2} alignItems="center">
          {fields.map(field => renderFieldset(field))}
        </Grid>
      </Form>
    </Container>
  );
}

export default GenericForm;
