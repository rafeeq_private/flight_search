import React from "react";
import { Button, Typography, Box } from "@material-ui/core";

const FlightTypeFilter = props => {
  return (
    <>
      <Typography mr={2} variant="subtitle1" color="secondary">
        Show Only
      </Typography>
      <Box ml={2}>
        {props.items.map(item => (
          <React.Fragment key={item}>
            <Button
              variant="outlined"
              color={props.flightType === item ? "primary" : "default"}
              onClick={() => props.onChange(item)}
            >
              {item}
            </Button>{" "}
          </React.Fragment>
        ))}
      </Box>
    </>
  );
};

export default FlightTypeFilter;
