import React from 'react';
import PropTypes from 'prop-types';
import { Toolbar, Container, Typography, AppBar } from '@material-ui/core';

const TopBar = props => (
    <AppBar position="static" color="secondary">
      <Container>
        <Toolbar>
          <Typography variant="h4">{props.title}</Typography>
        </Toolbar>
      </Container>
    </AppBar>
)

AppBar.propTypes= {
    title: PropTypes.string
}

export default TopBar