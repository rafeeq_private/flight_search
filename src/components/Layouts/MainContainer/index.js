import React from "react";
import { Container } from "@material-ui/core";
import TopBar from "../TopBar";

const MainContainer = props => (
  <>
    <TopBar title="Flights" />
    <Container>{props.children}</Container>
  </>
);
export default MainContainer;