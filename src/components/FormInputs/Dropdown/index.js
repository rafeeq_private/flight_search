import React from "react";
import {
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  FormHelperText
} from "@material-ui/core/";

export default function Dropdown(props) {
  const {
    meta: { touched, error }
  } = props;

  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel>{props.label}</InputLabel>
      <Select {...props.input} error={touched && error ? true : false}>
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {props.options.map((option, index) => (
          <MenuItem key={index} value={option[props.optionValueKey]}>
            {option[props.optionLabelKey]}
          </MenuItem>
        ))}
      </Select>
      {touched && error && <FormHelperText error>{error}</FormHelperText>}
    </FormControl>
  );
}
