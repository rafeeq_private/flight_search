import React from "react";
import {
  KeyboardDateTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";

function DatePicker(props) {

  return (
    <>
      {props.showTime ? (
        <KeyboardDateTimePicker
          {...props.input}
          variant="inline"
          format="DD/MM/YYYY hh:mm"
          inputVariant="outlined"
          label={props.label}
          fullWidth
          autoOk
        />
      ) : (
        <KeyboardDatePicker
          {...props.input}
          variant="inline"
          format="DD/MM/YYYY"
          inputVariant="outlined"
          label={props.label}
          fullWidth
          autoOk
        />
      )}
    </>
  );
}

export default DatePicker;
