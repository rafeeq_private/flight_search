import React from "react";
import TextField from "@material-ui/core/TextField";
import { Autocomplete as MuiAutocomplete } from "@material-ui/lab/";

export default function Autocomplete(props) {
  return (
    <MuiAutocomplete
      options={props.options}
      getOptionLabel={option => option[props.optionLabelKey]}
      renderInput={params => (
        <TextField
          {...props.input}
          {...params}
          label={props.label}
          variant="outlined"
        />
      )}
    />
  );
}
