import React from "react";
import TextField from "@material-ui/core/TextField";

export default function TextInput(props) {
  const {meta: { touched, error } } = props
  return (
    <TextField
      {...props.input}
      label={props.label}
      variant="outlined"
      fullWidth
      error={touched && error ? true : false}
      helperText={touched && error}
    />
  );
}
