import React from "react";
import { Button as MuiButton } from "@material-ui/core";

export default function Button(props) {
  return (
    <MuiButton {...props.inputProps} color="secondary">
      {props.label}
    </MuiButton>
  );
}
