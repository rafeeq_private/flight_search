import React from "react";
import {
  FormControl,
  Radio,
  RadioGroup as MuiRadioGroup,
  FormControlLabel
} from "@material-ui/core";

export default function RadioGroup(props) {
  return (
    <FormControl component="fieldset">
      <MuiRadioGroup
        {...props.alignment}
        {...props.input}
      >
        <FormControlLabel value="oneway" control={<Radio />} label="One Way" />
        <FormControlLabel
          value="round"
          control={<Radio />}
          label="Round Trip"
        />
      </MuiRadioGroup>
    </FormControl>
  );
}
