import React from "react";
import { Button, Typography, Box } from "@material-ui/core";
import startCase from "lodash/startCase";

const FlightSort = props => {
  return (
    <>
      <Typography mr={2} variant="subtitle1" color="secondary">
        Sort By
      </Typography>
      <Box ml={2}>
      {props.sorts.map(item => (
        <React.Fragment key={item}>
          <Button
            variant="outlined"
            color={item === props.sortKey ? "primary" : "default"}
            onClick={() => {
              props.onKeyChange(item);
              props.sortOrder === "asc"
                ? props.onOrderChange("desc")
                : props.onOrderChange("asc");
            }}
          >
            {startCase(item)}
          </Button>{" "}
        </React.Fragment>
      ))}
      </Box>
    </>
  );
};

export default FlightSort;
