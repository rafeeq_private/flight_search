import React, { useState } from "react";
import GenericForm from "components/FormGenerator";
import { useDispatch } from "react-redux";
import moment from "moment";
import { addFlightToForm } from "redux/actions";
import { reset } from "redux-form";
import Alert from "@material-ui/lab/Alert";
import {
  Container,
  DialogTitle,
  Dialog,
  DialogContent,
  Button,
  Box,
  IconButton
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
}));

const getFields = () => {
  return [
    {
      name: "departure",
      label: "Departure Airport",
      type: "text",
      validate: ["required"],
      grid: {
        xs: 12
      }
    },
    {
      name: "arrival",
      label: "Destination Airport",
      type: "text",
      validate: ["required"],
      grid: {
        xs: 12
      }
    },
    {
      name: "departureTime",
      label: "Departure Time",
      type: "datePicker",
      showTime: true,
      grid: {
        xs: 12,
        md: 6
      }
    },
    {
      name: "arrivalTime",
      label: "Arrival Time",
      type: "datePicker",
      showTime: true,
      grid: {
        xs: 12,
        md: 6
      }
    },
    {
      name: "type",
      label: "Type",
      type: "dropDown",
      grid: {
        xs: 12
      },
      validate: ["required"],
      options: [
        {
          label: "Business",
          value: "business"
        },
        {
          label: "Cheap",
          value: "cheap"
        }
      ],
      optionLabelKey: "label",
      optionValueKey: "value"
    },
    {
      name: "submit",
      label: "Save",
      type: "button",
      inputProps: {
        type: "submit",
        variant: "contained",
        color: "primary"
      },
      grid: {
        xs: 12
      }
    }
  ];
};

const initialValues = {
  departure: "",
  arrival: "",
  departureTime: moment(),
  arrivalTime: moment(),
  type: ""
};

export default function AddFlight(props) {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState(false);
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const addFlight = values => {
    values.departureTime = new Date(values.departureTime).getTime() / 1000;
    values.arrivalTime = new Date(values.arrivalTime).getTime() / 1000;

    dispatch(addFlightToForm(values));
    dispatch(reset("addFlight"));
    setMessage("New flight added");
  };

  return (
    <>
      <Button variant="contained" onClick={handleClickOpen}>
        Add New Flight
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Add new flight
          <IconButton
            className={classes.closeButton}
            aria-label="close"
            onClick={handleClose}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          {message && (
            <Container>
              <Box mb={2}>
                <Alert
                  action={
                    <Button
                      onClick={() => setMessage(false)}
                      color="inherit"
                      size="small"
                    >
                      OK
                    </Button>
                  }
                  severity="success"
                  variant="filled"
                >
                  {message}
                </Alert>
              </Box>
            </Container>
          )}

          <GenericForm
            formId="addFlight"
            fields={getFields()}
            initialValues={initialValues}
            onSubmit={addFlight}
          />
        </DialogContent>
      </Dialog>
    </>
  );
}
