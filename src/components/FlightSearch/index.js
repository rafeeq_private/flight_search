import React from "react";
import GenericForm from "components/FormGenerator";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { setSearchParams } from "core/actions";
import _ from "lodash";

const FlightSearchForm = props => {
  const flights = useSelector(state => state.flights).toJS();
  const dispatch = useDispatch();

  const airportList = flights
    .map(flight => ({
      airport: flight.departure
    }))
    .concat(
      flights.map(flight => ({
        airport: flight.arrival
      }))
    );

  const uniqAirportList = _.uniqBy(airportList, "airport");

  const getFields = () => {
    return [
      {
        name: "from",
        label: "Where from ?",
        type: "autoComplete",
        grid: {
          xs: 12
          // md: 6
        },
        options: uniqAirportList,
        optionLabelKey: "airport"
      },
      {
        name: "to",
        label: "Where to ?",
        type: "autoComplete",
        grid: {
          xs: 12
          // md: 6
        },
        options: uniqAirportList,
        optionLabelKey: "airport"
      },
      {
        name: "departure",
        label: "Date From",
        type: "datePicker",
        grid: {
          xs: 12
          // md: 6
        }
      },
      {
        name: "return",
        label: "Date To",
        type: "datePicker",
        grid: {
          xs: 12
          // md: 6
        }
      },
      {
        name: "submit",
        label: "Search",
        type: "button",
        inputProps: {
          type: "submit",
          variant: "contained",
          color: "primary"
        },
        grid: {
          xs: 12
        }
      }
    ];
  };

  const initialValues = {
    from: "",
    to: "",
    departure: moment("01/01/2018", "DD/MM/YYYY"),
    return: moment()
  };

  const searchFlight = values => {
    dispatch(setSearchParams(values));
  };

  return (
    <GenericForm
      formId="FlightSearch"
      fields={getFields()}
      initialValues={initialValues}
      onSubmit={searchFlight}
    />
  );
};

export default FlightSearchForm;
