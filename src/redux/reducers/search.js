import * as types from "../actionTypes.js";

export default function(state = {}, action) {
  switch (action.type) {
    case types.SET_SEARCH_PARAMS:
      return {...action.payload};
    default:
      return state;
  }
}
