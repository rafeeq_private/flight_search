import * as types from "../actionTypes.js";
import { List } from "immutable";

export default function(state = List(), action) {
  switch (action.type) {
    case types.ADD_FLIGHT:
      return state.concat(action.payload);
    case types.ADD_FLIGHT_TO_FORM:
      return state.push(action.payload);
    default:
      return state;
  }
}