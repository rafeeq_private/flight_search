import { combineReducers } from "redux";
import flights from './flights';
import search from './search';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    flights,
    search,
    form: formReducer
})