import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers";
import flightSaga from "./saga/saga";
import createSagaMiddleware from "redux-saga";

const sagaMiddleware = createSagaMiddleware();

export default createStore(
  rootReducer,
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // on dev mode
  )
);

sagaMiddleware.run(flightSaga);