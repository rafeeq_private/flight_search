import { call, put, takeLatest } from "redux-saga/effects";
import { FLIGHT_LIST_REQUESTED } from "../actionTypes";
import { get } from "../../axios-client";
import { addFlight } from "../actions";
import map from "lodash/map";

const prepareCheapFlights = flight => {
  const routes = flight.route.split("-");
  return {
    departure: routes[0],
    arrival: routes[1],
    departureTime: flight.departure,
    arrivalTime: flight.arrival,
    type: "cheap"
  };
};

const prepareBusinessFlights = flight => ({...flight, type: "business"});

function* fetchFlights() {
  try {
    const cheapFlights = yield call(get, `/flights/cheap`);
    yield put(addFlight(map(cheapFlights.data.data, prepareCheapFlights)));
    const businessFlights = yield call(get, `/flights/business`);
    yield put(addFlight(map(businessFlights.data.data, prepareBusinessFlights)));
  } catch (e) {
    console.warn(e); // TODO: show error
  }
}

function* flightSaga() {
  yield takeLatest(FLIGHT_LIST_REQUESTED, fetchFlights);
}

export default flightSaga;