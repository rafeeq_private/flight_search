import {
  FLIGHT_LIST_REQUESTED,
  ADD_FLIGHT,
  ADD_FLIGHT_TO_FORM,
  SET_SEARCH_PARAMS
} from "./actionTypes";
import { fromJS } from "immutable";

export const requestFlightList = () => ({
  type: FLIGHT_LIST_REQUESTED
});

export const addFlight = data => {
  return {
    type: ADD_FLIGHT,
    payload: fromJS(data)
  };
};

export const addFlightToForm = data => {
  return {
    type: ADD_FLIGHT_TO_FORM,
    payload: fromJS({ ...data })
  };
};

export const setSearchParams = data => {
  return {
    type: SET_SEARCH_PARAMS,
    payload: data
  };
};