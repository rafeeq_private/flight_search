export const ADD_FLIGHT = "ADD_FLIGHT"; 
export const SET_FILTER = "SET_FILTER";
export const FLIGHT_LIST_REQUESTED = "FLIGHT_LIST_REQUESTED";
export const ADD_FLIGHT_TO_FORM = "ADD_FLIGHT_TO_FORM"
export const SET_SEARCH_PARAMS = "SET_SEARCH_PARAMS"